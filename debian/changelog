opus-tools (0.2-1) unstable; urgency=medium

  * New upstream version 0.2

  * B-D on opus support libraries
  * No more README.md to install
  * Update d/copyright
    + Regenerate d/copyright_hints
    + Update 'licenscheck' target.
      Thanks to Jonas Smedegaard <dr@jones.dk>

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 18 Jul 2022 08:57:14 +0200

opus-tools (0.1.10-3) unstable; urgency=medium

  * Switch to short-form dh
    * Bump dh compat to 13 (via debhelper-compat pseudo B-D)
  * Add autopkgtests
  * Drop opus-tool-dbg in favour of the auto-generated dbgsym package
  * Remove trailing whitespace from d/changelog
  * Machine readable d/copyright
    + Add 'licensecheck' target to d/rules
    + Generate d/copyright_hints
  * Declare that building this package does not require 'root' powers.
  * Switch to source-format 3.0 (Closes: #1007561)
  * Add d/watch
  * DEP14: use standard branch names
  * Add salsa-ci configuration
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 03 Jul 2022 22:04:41 +0200

opus-tools (0.1.10-2) unstable; urgency=medium

  * Salvaging package on behalf of the Multimedia Team (Closes: #1011280)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 10 Jun 2022 08:27:00 +0200

opus-tools (0.1.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/compat: 5 -> 7. (Closes: #965759)

 -- Adrian Bunk <bunk@debian.org>  Wed, 19 Jan 2022 16:18:25 +0200

opus-tools (0.1.10-1) unstable; urgency=medium

  * Improved handling of malformed input files to avoid crashes and other
    troublesome behavior.   Closes: #780160
  * Enable pcap support for opusrtp.
  * Fix channel mask in mono float WAV output, it now uses 4 (front center)
    instead of 1 (front left).  There is no standard saying exactly what this
    should be, but we're now more in line with what several other tools do in
    this case.
  * Fix support for files with more than 80 channels.

 -- Ron Lee <ron@debian.org>  Mon, 23 Jan 2017 15:57:22 +1030

opus-tools (0.1.9-1) unstable; urgency=medium

  * Add support for 32-bit float output from opusdec.
  * Make opusenc --discard-comments discard album art, add --discard-pictures.
  * Set correct lsb depth for WAV and AIFF input.
  * Fix 8-bit AIFF input.

 -- Ron Lee <ron@debian.org>  Sat, 04 Oct 2014 13:37:31 +0930

opus-tools (0.1.8-1) unstable; urgency=low

  * Don't overlink everything with FLAC, only opusenc uses it at present.
  * Fixes copying pictures from FLAC with a mimetype.

 -- Ron Lee <ron@debian.org>  Mon, 09 Dec 2013 00:59:41 +1030

opus-tools (0.1.6+20130713-1) unstable; urgency=low

  * Preview snapshot for use with opus 1.1-beta and the new
    surround encoding improvements.
  * Adds support for encoding directly from FLAC.

 -- Ron Lee <ron@debian.org>  Sat, 13 Jul 2013 19:56:45 +0930

opus-tools (0.1.2-1) unstable; urgency=low

  * Better error detection and reporting for a number of cases.

 -- Ron Lee <ron@debian.org>  Wed, 13 Jun 2012 10:05:07 +0930

opus-tools (0.1.0+20120529-1) unstable; urgency=low

  * Initial release.  Closes: #674292

 -- Ron Lee <ron@debian.org>  Tue, 22 May 2012 01:38:18 +0930
